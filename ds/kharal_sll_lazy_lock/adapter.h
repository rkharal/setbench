#ifndef sll_lazy_lock_H
#define	sll_lazy_lock_H

//#define USE_TREE_STATS 0

#include <iostream>
#include <csignal>
#include <mutex>
#include "errors.h"
#include "random_fnv1a.h"

#ifdef USE_TREE_STATS
#   include "tree_stats.h"
#endif
#include "sll_lazy_lock_impl.h"

#define RECORD_MANAGER_T record_manager<Reclaim, Alloc, Pool, sll_lazy_lock_ns::Node<K, V>>
#define DATA_STRUCTURE_T sll_lazy_lock_ns::sll_lazy_lock<K, V, std::less<K>, RECORD_MANAGER_T>

//template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
class ds_adapter {
private:
    const V NO_VALUE;
    DATA_STRUCTURE_T * const ds;

public:
    ds_adapter(const int NUM_THREADS,
               const K& MIN_KEY,  //min
               const K& MAX_KEY,       //max
               const V& VALUE_RESERVED,
               RandomFNV1A * const unused2)
    : NO_VALUE(VALUE_RESERVED)
    , ds(new DATA_STRUCTURE_T(MIN_KEY, MAX_KEY, MIN_KEY, NO_VALUE, NUM_THREADS))
    {}
    ~ds_adapter() {
        delete ds;
    }
    
    V getNoValue() {
        return NO_VALUE;
    }
    
    void initThread(const int tid) {
        ds->initThread(tid);
    }
    void deinitThread(const int tid) {
        ds->deinitThread(tid);
    }

    bool contains(const int tid, const K& key) {
        return ds->contains(tid, key);
    }
    /* sll_lazy_lock_ns::nodeptr gethead() {
        return ds->getHead();
    } */
    
    V insert(const int tid, const K& key, const V& val) {
        return ds->insert(tid, key, val);
    }
    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return ds->insertIfAbsent(tid, key, val);
    }
    V erase(const int tid, const K& key) {
        return ds->erase(tid, key).first;
    }
    V find(const int tid, const K& key) {
        return ds->find(tid, key).first;
    }
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) {
        return ds->rangeQuery(tid, lo, hi, resultKeys, resultValues);
    }
    void printSummary() {
        auto recmgr = ds->debugGetRecMgr();
        recmgr->printStatus();
    }
    bool validateStructure() {
        return true;
    }
    void printObjectSizes() {
        std::cout<<"sizes: node="
                 <<(sizeof(sll_lazy_lock_ns::Node<K, V>))
                 <<std::endl;
    }



#ifdef USE_TREE_STATS
class NodeHandler {
    public:
        typedef sll_lazy_lock_ns::Node<K, V> * NodePtrType;
        K minKey;
        K maxKey;
        
        NodeHandler(const K& _minKey, const K& _maxKey) {
            minKey = _minKey;
            maxKey = _maxKey;
        }
        
        class ChildIterator {
        private:
            NodePtrType node; // node being iterated over
            bool called_next;
        public:
            ChildIterator(NodePtrType _node) {
                node = _node;
                called_next = false;
            }
            
            bool hasNext() {
                
                return node->next != NULL && !(called_next);
            }
            
            NodePtrType next() {
               called_next = true;
               return node->next;

            }
        };
        
        bool isLeaf(NodePtrType node) {
            return node->next== NULL ? true : false;
        }
        size_t getNumChildren(NodePtrType node) {
           // return node->next == NULL ? 0 : 1;
           if ( node->next== NULL) {
               return 0;
           } else
           {
               return 1;
           } 
        }
        size_t getNumKeys(NodePtrType node) {
            // check for min or max key
            if ((node->key == minKey) ||  (node->key == maxKey)) {
                return 0;
            }
            return 1;
        }
        
        size_t getSumOfKeys(NodePtrType node) {
             if( (node->key == minKey) ||  (node->key == maxKey)) {
                return 0;
                
            }
            return (size_t) node->key;
        }
        ChildIterator getChildIterator(NodePtrType node) {
            return ChildIterator(node);
        }
    };
    TreeStats<NodeHandler> * createTreeStats(const K& _minKey, const K& _maxKey) {
        return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getHead(), true);
    }
#endif
};

#endif