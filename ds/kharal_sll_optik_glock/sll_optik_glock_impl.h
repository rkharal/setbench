

#ifdef PRINTX
#define STR(x) XSTR(x)
#define XSTR(x) #x
#define PRINTINT(name)                                    \
    {                                                   \
        std::cout << #name << "=" << name << std::endl; \
    }
#define PRINTX(name)                                         \
    {                                                        \
        std::cout << #name << "=" << STR(name) << std::endl; \
    }
#else
#define PRINTX
#define PRINTINT

#endif
//#ifndef sll_optik_glock_H
//#define	sll_optik_glock_H
#include "record_manager.h"
#include "random_fnv1a.h"
#include "locks_impl.h"
#include "optik.h"

using namespace std;

namespace sll_optik_glock_ns
{

#define nodeptr Node<K, V> *

template <class K, class V>
class Node
{
public:
    V value;
    K key;
    nodeptr next;
    //nodeptr right;
};

template <class K, class V, class Compare, class RecManager>
class sll_optik_glock
{
private:
    PAD;
    RecManager *const recmgr;
    //PAD;
    //volatile int lock;
    PAD;
    //nodeptr root;        // actually const
    Compare cmp;

    PAD;
    nodeptr head;
    PAD;
    nodeptr tail;
    PAD;
    optik_lock oglock;
    PAD;
    //simple_record_manager<Node> * recmgr;
    //int numThreads;
    PAD;

    PAD;
    inline nodeptr createNode(const int tid, const K &key, const V &value, nodeptr const next)
    {
        nodeptr newnode = recmgr->template allocate<Node<K, V>>(tid);
        if (newnode == NULL)
        {
            COUTATOMICTID("ERROR: could not allocate node" << std::endl);
            exit(-1);
        }
#ifdef __HANDLE_STATS
        GSTATS_APPEND(tid, node_allocated_addresses, ((long long)newnode) % (1 << 12));
#endif
        newnode->key = key;
        newnode->value = value;
        newnode->next = next;
        //newnode->right = right;
        return newnode;
    }

    //define acquiring and releasing the optik lock as private functions here

    void release_optik_lock()
    {
        oglock.optik_unlock();
    }

    int acquire_optik_lock()
    {
        bool islocked = false;
        uint64_t curr_version;
        // first assure that the lock is obtained - busy wait otherwise
        while (!islocked)
        {
            curr_version = oglock.optik_get_version();
            islocked = oglock.optik_trylock(curr_version);
        }
        return curr_version + 1;
        //int locked_version = oglock.optik_get_version();  //curr_version + 1;
        //this is new version number
    }

    bool insertNode(const int tid, const K &key, const V &val, nodeptr prev);
    const V doInsert(const int tid, const K &key, const V &val, bool onlyIfAbsent);
    bool doErase(const int tid, nodeptr prev, nodeptr node);
    PAD;

    int init[MAX_THREADS_POW2] = {
        0,
    };
    PAD;

public:
    const K NO_KEY;
    const V NO_VALUE;
    PAD;

    /**
         * This function must be called once by each thread that will
         * invoke any functions on this class.
         * 
         * It must be okay that we do this with the main thread and later with another thread!!!
         */
    void initThread(const int tid)
    {
        if (init[tid])
            return;
        else
            init[tid] = !init[tid];
        recmgr->initThread(tid);
    }
    void deinitThread(const int tid)
    {
        if (!init[tid])
            return;
        else
            init[tid] = !init[tid];
        recmgr->deinitThread(tid);
    }

    sll_optik_glock(const K MIN_KEY, 
                    const K MAX_KEY, 
                    const K _NO_KEY,
                    const V _NO_VALUE,
                    const int numProcesses)
    : NO_KEY(_NO_KEY), NO_VALUE(_NO_VALUE), recmgr(new RecManager(numProcesses))
    //: NO_VALUE(_NO_VALUE), recmgr(new RecManager(numProcesses))
    {
        //lock = 0;

        VERBOSE DEBUG COUTATOMIC("constructor sll_optik_glock" << std::endl);
        cmp = Compare();

        const int tid = 0;
        initThread(tid);

        recmgr->endOp(tid); // enter an initial quiescent state.
        //nodeptr rootleft = createNode(tid, NO_KEY, NO_VALUE, NULL);
        //nodeptr _root = createNode(tid, NO_KEY, NO_VALUE, rootleft);
        //root = _root;

        oglock.optik_version_init();
        //printf(" Max %i, Min %i \n", (uint)MAX_KEY, (uint)MIN_KEY);

        tail = createNode(tid, MAX_KEY, NO_VALUE, NULL);
        head = createNode(tid, MIN_KEY, NO_VALUE, tail);
        //head->next =tail;
        PRINTX("in constructor");
    }

    // ?? shouldnt this be called recursively
    void dfsDeallocateBottomUp(nodeptr const u, int *numNodes)
    {
        if (u == NULL)
            return;
        if (u->next != NULL)
        {

            dfsDeallocateBottomUp(u->next, numNodes);
            //dfsDeallocateBottomUp(u->right, numNodes);
        }
        MEMORY_STATS++(*numNodes);
        const int tid = 0;
        recmgr->deallocate(tid, u);
    }
    ~sll_optik_glock()
    {
        VERBOSE DEBUG COUTATOMIC("destructor sll_optik_glock");
        // free every node and scx record currently in the data structure.
        // an easy DFS, freeing from the leaves up, handles all nodes.
        // cleaning up scx records is a little bit harder if they are in progress or aborted.
        // they have to be collected and freed only once, since they can be pointed to by many nodes.
        // so, we keep them in a set, then free each set element at the end.
        int numNodes = 0;
        dfsDeallocateBottomUp(head, &numNodes);
        VERBOSE DEBUG COUTATOMIC(" deallocated nodes " << numNodes << std::endl);
        recmgr->printStatus();
        delete recmgr;
    }

    const V insert(const int tid, const K &key, const V &val) { return doInsert(tid, key, val, false); }
    const V insertIfAbsent(const int tid, const K &key, const V &val) { return doInsert(tid, key, val, true); }
    const std::pair<V, bool> erase(const int tid, const K &key);
    const std::pair<V, bool> find(const int tid, const K &key);
    int rangeQuery(const int tid, const K &lo, const K &hi, K *const resultKeys, V *const resultValues) { return 0; }
    bool contains(const int tid, const K &key) { return find(tid, key).second; }
    nodeptr getHead() { return head; }
    RecManager *debugGetRecMgr() { return recmgr; }
    nodeptr debug_getEntryPoint() { return head; }
    K getSumOfKeys();
    void printDebuggingDetails();

};

} // namespace sll_optik_glock_ns

template <class K, class V, class Compare, class RecManager>
const std::pair<V, bool> sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::find(const int tid, const K &key)
{
    auto guard = recmgr->getGuard(tid, true);

    nodeptr currNode = head;
    K currKey = 0;
    while (true)
    {

        currKey = currNode->key;
       // PRINTI(currKey);

        if (key == currKey)
        {
            break;
            //return true;
        }

        currNode = currNode->next;
        if (currNode == tail)
        {
            break; //return false;
        }
    }

    auto result = (key == currKey) ? std::pair<V, bool>(currNode->value, true) : std::pair<V, bool>(NO_VALUE, false);
    return result;

   
}

template <class K, class V, class Compare, class RecManager>
const V sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::doInsert(const int tid, const K &key, const V &val, bool onlyIfAbsent)
{
    

    while (true)
    {  

        auto guard = recmgr->getGuard(tid);

        acquire_optik_lock();

        nodeptr prev = head;
        nodeptr currNode = head->next;
        while (true)
        {

            K currKey = currNode->key;

            PRINTX("Do Insert");
            PRINTINT(currKey);
            PRINTINT(key);
            if (key == currKey)
            {
                //printf(" DUPLICATE lock thread %i, Before UNLOCK Version %i \n", tid, (int)oglock.optik_get_version());

                V result = currNode->value;
                if (!onlyIfAbsent)
                {
                    currNode->value = val;
                }
                PRINTX("Found duplicate");
                PRINTINT(currKey);
                release_optik_lock();
                return result;
            }
            else if (cmp(key,currKey)) // This was correct without the cmp function "(currKey > key)"" 
            {
                if (insertNode(tid, key, val, prev))
                {
                    //printf(" After DoInsert Success lock thread %i, Before UNLOCK Version %i \n", tid, (int)oglock.optik_get_version());
                    release_optik_lock();
                    PRINTX("Insert successful");
                    PRINTINT(currKey);
                    return NO_VALUE;
                }
                //if insert failed we must try again from beginning
                break;
            }
            prev = currNode;
            currNode = currNode->next;
        }

        assert(currNode);
        release_optik_lock();

        /* if(currNode == tail && doInsert(tid, prev, key)){
            //printf(" TAIL lock thread %i, Before UNLOCK Version %i \n", tid, (int)oglock.optik_get_version());
            release();
            return true;
        } //if we fail we try insert all over again
           */
    }
}


template <class K, class V, class Compare, class RecManager>
bool sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::insertNode(const int tid, const K &key, const V &val, nodeptr prev)

{

    nodeptr next = prev->next;
    nodeptr newnode = createNode(tid, key, val, next);
    

    prev->next = newnode;
    return true;

    // only if fail : recmgr->deallocate(tid, newNode);
    // assume since it is a global lock it cannot fail
}


template <class K, class V, class Compare, class RecManager>
const std::pair<V, bool> sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::erase(const int tid, const K &key)
{
    // semantics: try to erase key. return true if successful, and false otherwise

    while (true)
    {
        auto guard = recmgr->getGuard(tid);

        acquire_optik_lock();

        nodeptr prev = head;
        nodeptr currNode = head->next;
        while (true)
        {
            K currKey = currNode->key;

            if (key == currKey)
            {
                V result = currNode->value;
                if (doErase(tid, prev, currNode))
                {
                    release_optik_lock();
                    return std::pair<V, bool>(result, true);
                }
                break;
            }
            else if(cmp(key, currKey))   //(key < currKey)
            {
                release_optik_lock();
                return std::pair<V, bool>(NO_VALUE, false);
            }
            prev = currNode;
            currNode = currNode->next;
        }
        release_optik_lock();
    }
}

template <class K, class V, class Compare, class RecManager>
bool sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::doErase(const int tid, nodeptr prev, nodeptr node)
{

    nodeptr next = node->next;

    prev->next = next;

    //node->next =NULL;

    //cant deallocate now since readers might be reading this node
    recmgr->retire(tid, node);
    return true;
}

template <class K, class V, class Compare, class RecManager>
K sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::getSumOfKeys()
{

// semantics:  */
//return the sum of all KEYS in the set
    nodeptr currNode = head->next;
    int64_t sum = 0;
    while (currNode != tail) {
        sum += currNode->key;
        currNode = currNode->next;
    }
    return (K)sum;
} 

template <class K, class V, class Compare, class RecManager>
void sll_optik_glock_ns::sll_optik_glock<K, V, Compare, RecManager>::printDebuggingDetails()
{
//void sll_optik_glock::printDebuggingDetails() {
/* 
    Node * temp = head;
    int i =0;
    while (true){
        int key = temp->key;
       // printf("Node i %i has key: %i \n", i, key );    
        i++;
        temp = temp->next;
        if (temp==tail) break;
        
    }
     * 
     */
}

//#endif