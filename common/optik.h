#include <atomic>
#include "assert.h"
#ifndef OPTIK_H
#define OPTIK_H

#define VERSION_INIT 0
#define VERSION_LOCKED 0x1LL

typedef std::atomic<uint64_t> optik_t;

class optik_lock{
private:
    optik_t version;

public:
    void optik_version_init(){
        version = VERSION_INIT;    
    }
            
    bool optik_trylock(uint64_t targetv){
        if ((version & VERSION_LOCKED) || (targetv != version)) {
            return false;
        }
        //assert ((version & VERSION_LOCKED));
        //if a thread makes it here and the version changed in between then the
        // CAS will fail
        //cout<<" TRYLOCK current version :"<< (int)(version)<< endl;
        return version.compare_exchange_strong(targetv,targetv+1);
        //cout<<" TRYLOCK after CAS version :"<< (int)(version)<< endl;
        //return true;
    }

    //NOT SURE I NEED THIS FUNCTION
    bool optik_lock_version(uint64_t targetv){
    if ((version & VERSION_LOCKED) || (targetv != version)) {
            return false;
        }
        
        return version.compare_exchange_strong(targetv,targetv+1);
    }

    int optik_get_version(){
        return version;
    }

    int optik_unlock(){
        uint64_t val = version;
        assert(val & VERSION_LOCKED);
        version = val+1;
        
        // //Interesting Mistake Here: I initially  used version.store.
        // //thinking that only one thread will ever UNLOCK.
        // version.compare_exchange_strong(curr_version,curr_version +1);
        // //cout<<" UNLOCKED the lock: NOW version i: "<< (int)(version)<< endl;
    }

    int optik_revert(optik_t v, optik_t targetv){
            
    }

    int optik_islocked(optik_t v, optik_t targetv){
        return (version & VERSION_LOCKED);
    }
};

#endif 